# markup-boilerplate
(v 0.0.7) - No description yet

## How to use
```
yarn run gulp
```

## todo

* add image compression

#### Scripts

* babel/core
* babel/preset-env
* babel-loader
* webpack
* webpack-stream

#### Styles

* gulp-sass
* gulp-autoprefixer

#### Clean

* del

#### Markup

* gulp-file-include

#### General

* browser-sync
* chalk
* gulp
* gulp-if
* gulp-load-plugins
* gulp-require-tasks
* run-sequence
* gulp-plumber